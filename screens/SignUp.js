import { ScreenContainer } from "react-native-screens";
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import React from 'react';
import { TextInput } from 'react-native-paper';


export const SignUp = ({ navigation }) => {
    return (
        <ScreenContainer>

            <View style={styles.container}>
            <Image style={[styles.logo]}source={require('../assets/logo1.png')}/>
                <View style={styles.title}>
                    <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Buat Sebuah Akun</Text>
                </View>
                <View>
                    <TextInput label='NIP' style={[styles.input]} />
                    <TextInput label='Username' style={[styles.input]} />
                    <TextInput label='Password' style={[styles.input]} />
                    <TouchableOpacity style={[styles.submitButton]}>
                        <Text style={{ color: '#fff', textAlign: "center", fontWeight: 'bold' }}>Daftar!</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity title="Create Account" onPress={() => navigation.push('SignIn')}>
                    <Text style={styles.redirectText}>Sudah Punya Akun? Masuk Sekarang</Text>
                </TouchableOpacity>
            </View>
        </ScreenContainer>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        marginLeft: 40,
        marginRight: 40,
    },
    title: {
        marginBottom: 40,
    },
    input: {
        marginBottom: 12,
        backgroundColor: 'transparent',
    },
    submitButton: {
        marginBottom: 40,
        marginTop: 40,
        backgroundColor: '#5F6CAF',
        borderRadius: 15,
        width: 200,
        alignSelf: "center",
        padding: 15,
    },
    redirectText: {
        fontSize:11,
        textAlign:"center",
    },
    logo: {
        width: 50, 
        height: 63, 
        alignSelf:"center", 
        marginBottom:60,
        marginTop: 30
    }
});