import { ScreenContainer } from "react-native-screens";
import {View,Text,StyleSheet,TouchableOpacity,Image} from 'react-native';
import React from 'react';
import { TextInput } from 'react-native-paper';



export const SignIn = ({ navigation }) => {
    return (
        <ScreenContainer>
            <View style={styles.container}>
            <Image style={[styles.logo]}source={require('../assets/logo1.png')}/>
                <View style={styles.title}>
                    <Text style={{ fontSize: 25 }}>Selamat Datang,</Text>
                    <Text style={{ fontSize: 25, fontWeight:'bold' }}>Masuk Untuk Melanjuti</Text>
                </View>
                <View>
                    <TextInput label='Username' style={[styles.input]}/>
                    <TextInput label='Password' style={[styles.input]}/>
                    <TouchableOpacity style={[styles.submitButton]}>
                        <Text style={{ color:'#fff', textAlign:"center", fontWeight:'bold' }}>Masuk!</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity title="Create Account" onPress={() => navigation.push('SignUp')}>
                    <Text style={styles.redirectText}>Belum Punya Akun? Daftar Terlebih Dahulu</Text>
                </TouchableOpacity>
            </View>
        </ScreenContainer>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        marginLeft: 40,
        marginRight: 40,
    },
    title: {
        marginBottom: 40,
    },
    input:{
        marginBottom:12,
        backgroundColor:'transparent',
    },
    submitButton: {
        marginBottom:40,
        marginTop:30,
        backgroundColor:'#5F6CAF', 
        borderRadius:15, 
        width: 200, 
        alignSelf:"center",
        padding:15,
    },
    redirectText: {
        fontSize:11,
        textAlign:"center",
    },
    logo: {
        width: 50, 
        height: 63, 
        alignSelf:"center", 
        marginBottom:60,
        marginTop: 30
    }
});